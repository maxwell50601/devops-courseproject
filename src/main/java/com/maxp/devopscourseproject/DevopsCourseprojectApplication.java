package com.maxp.devopscourseproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsCourseprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsCourseprojectApplication.class, args);
	}

}

